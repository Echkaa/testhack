<?php

Auth::routes();

Route::get('/', 'HomeController@index');

Route::get('prod/add', 'ProdController@create');
Route::get('prod/{prod}/delete', [
    'as'   => 'prod.delete',
    'uses' => 'ProdController@destroy',
]);
Route::resource('/prod', 'ProdController');

Route::get('recipes/add', 'RecipesController@create');
Route::get('recipes/{recipes}/delete', [
    'as'   => 'recipes.delete',
    'uses' => 'RecipesController@destroy',
]);
Route::resource('/recipes', 'RecipesController');


function is_active_sorter($key, $direction = 'ASC')
{
    if (request('sortby') == $key && request('sortdir') == $direction) {
        return true;
    }

    return false;
}


