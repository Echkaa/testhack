<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    public $timestamps = false;

    protected $fillable = ['user_id', 'count', 'count_type', 'name', 'content', 'created_at', 'finish_prod'];

    public $typeCount = [
        'грам',
        'кг',
        'мл',
        'л'
    ];

    public $typeKoef = [
        'грам' => 1,
        'кг' => 1000,
        'мл' => 1,
        'л' => 1000
    ];
}
