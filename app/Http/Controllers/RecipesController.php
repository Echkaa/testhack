<?php

namespace App\Http\Controllers;

use App\Http\Requests\PageFormRequest;
use App\Product;
use App\ProductContent;
use App\Recipes;
use App\RecipesComponent;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RecipesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sortBy = 'id';
        $sortDirection = 'ASC';

        if (request('sortby') || request('sortdir')) {
            $sortBy = request('sortby');
            $sortDirection = request('sortdir');
        }

        $coef = new Product;
        $resipes = Recipes::orderBy($sortBy, $sortDirection)->paginate(10);
        $user = Auth::user();

        return view('resipes/index', ['resipes' => $resipes, 'user' => $user, 'coef' => $coef]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $resipes = new Recipes();
        $prod = new Product();

        return view('resipes/create', ['resipes' => $resipes, 'prod' => $prod]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $resipes = Recipes::create(
            [
                'title' => $request->title,
                'cooking_method' => $request->cooking_method,
                'complexity' => $request->complexity,
                'recipes' => $request->recipes,
            ]
        );

        if ($request['components']) {
            $i = 1;
            foreach ($request['components'] as $component) {
                RecipesComponent::create(
                    [
                        'recipes_id' => $resipes->id,
                        'name' => $request['components_name'][$i],
                        'count' => $component,
                        'count_type' => $request['components_type'][$i],
                        'possibility_of_replacement' => $request['components_change'][$i],
                    ]
                );
                $i++;
            }
        }

        alert()->success('Рецепт був доданий');

        return redirect()->route('recipes.edit', $resipes->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $resipes = Recipes::find($id);
        $coef = new Product;
        $user = Auth::user();

        return view('resipes/show', ['resipes' => $resipes, 'user' => $user, 'coef' => $coef]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $resipes = Recipes::find($id);
        $prod = new Product();

        return view('resipes/edit', ['resipes' => $resipes, 'prod' => $prod]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Recipes $recipes, Request $request)
    {
        $recipes = Recipes::updateOrCreate(
            [
                'id' => $request->id,
            ],
            [
                'title' => $request->title,
                'cooking_method' => $request->cooking_method,
                'complexity' => $request->complexity,
                'recipes' => $request->recipes,
            ]
        );

        if ($request['components']) {
            RecipesComponent::where('recipes_id', $recipes->id)->delete();
            $i = 1;
            foreach ($request['components'] as $component) {
                if (!isset($request['components_change'][$i])) {
                    $components_change = 'off';
                } else {
                    $components_change = $request['components_change'][$i];
                }
                RecipesComponent::create(
                    [
                        'recipes_id' => $recipes->id,
                        'name' => $request['components_name'][$i],
                        'count' => $component,
                        'count_type' => $request['components_type'][$i],
                        'possibility_of_replacement' => $components_change,
                    ]
                );
                $i++;
            }
        }

        alert()->success('Рецепт був оновлений');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Recipes::destroy($id);

        alert()->success('Рецепт був видалений');

        return redirect('/recipes');
    }
}
