<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductBarcode;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function store(Request $request) {
        Product::create(
            [
                'user_id' => $request->id,
                'count' => 0,
                'count_type' => 'кг',
                'name' => 'Автододавання після натискання кнопки',
                'content' => ' ',
                'created_at' => date('Y-m-d'),
                'finish_prod' => date('Y-m-d', time() + 86400 * 7),
            ]
        );
    }
}
