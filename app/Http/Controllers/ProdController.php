<?php

namespace App\Http\Controllers;

use App\Http\Requests\PageFormRequest;
use App\Product;
use App\ProductBarcode;
use App\ProductContent;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProdController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sortBy = 'id';
        $sortDirection = 'ASC';

        if (request('sortby') || request('sortdir')) {
            $sortBy = request('sortby');
            $sortDirection = request('sortdir');
        }

        $products = Product::where('user_id', Auth::user()->id)->orderBy($sortBy, $sortDirection)->paginate(10);

        return view('prod/index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $prod = new Product();

        return view('prod/create', compact('prod'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $content = '';

        $prodContent = ProductContent::where('product_name', $request->name)
            ->where('user_id', Auth::user()->id)
            ->orWhere('user_id', 0)
            ->limit(1)
            ->get();

        if ($prodContent) {
            $content = $prodContent[0]['description'];
        }

        $prod = Product::create(
            [
                'user_id' => Auth::user()->id,
                'count' => $request->count,
                'count_type' => $request->count_type,
                'name' => $request->name,
                'content' => $content,
                'created_at' => date('Y-m-d'),
                'finish_prod' => $request->finish_prod,
            ]
        );

        alert()->success('Продукт добавлен');

        return redirect()->route('prod.edit', $prod->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prod = Product::where('user_id', Auth::user()->id)->find($id);

        return view('prod/show', compact('prod'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prod = Product::where('user_id', Auth::user()->id)->find($id);

        return view('prod/edit', ['prod' => $prod]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Product $prod, Request $request)
    {
        if ($request->content != '') {
            $prodContent = ProductContent::where('product_name', $request->name)
                ->where('user_id', Auth::user()->id)
                ->get();

            if (isset($prodContent[0])) {
                ProductContent::where('product_name', $request->name)
                    ->update(['description' => $request->content]);
            } else {
                ProductContent::create([
                    'user_id' => Auth::user()->id,
                    'product_name' => $request->name,
                    'description'  => $request->content
                ]);
            }
        }

        Product::updateOrCreate(
            [
                'id' => $request->id,
                'user_id' => Auth::user()->id,
            ],
            [
                'count' => $request->count,
                'count_type' => $request->count_type,
                'name' => $request->name,
                'content' => $request->content,
                'finish_prod' => $request->finish_prod,
            ]
        );

        alert()->success('Продукт оновлений');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);

        alert()->success('Продукт видалений');

        return redirect('/prod');
    }
}
