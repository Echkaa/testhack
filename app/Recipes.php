<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipes extends Model
{
    protected $table = 'recipes';

    protected $fillable = [
        'title',
        'cooking_method',
        'complexity',
        'recipes'
    ];

    public $complexityTypes = [
        'Легко',
        'Досить складно',
        'Складно'
    ];

    public $cookingMethodTypes = [
        'Запікання',
        'Гасіння',
        'Приготування на пару',
        'Варка',
        'Смаження',
        'Копчення',
        'Приготування в мікрохвильовій печі',
        'Консервування',
        'Соління',
    ];

    public function components()
    {
        return $this->hasMany('App\RecipesComponent');
    }
}
