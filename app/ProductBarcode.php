<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductBarcode extends Model
{
    protected $table = 'product_barcode';

    public $timestamps = false;
}
