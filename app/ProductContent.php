<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductContent extends Model
{
    protected $table = 'product_content';

    public $timestamps = false;

    protected $fillable = ['product_name', 'description'];
}
