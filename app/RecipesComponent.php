<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipesComponent extends Model
{
    protected $table = 'recipes_components';

    protected $fillable = [
        'recipes_id',
        'name',
        'count',
        'count_type',
        'possibility_of_replacement',
    ];
}
