@extends('layouts.app')

@section('content')
  <style>
    .alert-danger {
      background-color: #f2dede !important;
    }
    .alert-warning {
      background-color: #fcf8e3 !important;
    }
  </style>

  <div class="container">

    <div class="clearfix">
      <div class="pull-left">
        <div class="lead">Продукти</div>
      </div>
      <div class="pull-right">
        <a href="/prod/add" class="btn btn-success">Додати</a>
      </div>
    </div>

    <hr>

    <div class="col-xs-12" style="overflow-x: auto">
    <table class="table table-bordered table-hover table-striped">
      <thead>
      <tr>
        <th class="col-xs-1">
          ID
          <div class="pull-right">
            <a href="?sortby=id&sortdir=ASC"
               class="btn btn-xs {{ is_active_sorter('id', 'ASC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-up"></i>
            </a>
            <a href="?sortby=id&sortdir=DESC"
               class="btn btn-xs {{ is_active_sorter('id', 'DESC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-down"></i>
            </a>
          </div>
        </th>
        <th class="col-xs-3">
          Найменування товару
          <div class="pull-right">
            <a href="?sortby=name&sortdir=ASC"
               class="btn btn-xs {{ is_active_sorter('name', 'ASC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-up"></i>
            </a>
            <a href="?sortby=name&sortdir=DESC"
               class="btn btn-xs {{ is_active_sorter('name', 'DESC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-down"></i>
            </a>
          </div>
        </th>
        <th class="col-xs-3">
          Кількість
          <div class="pull-right">
            <a href="?sortby=count&sortdir=ASC"
               class="btn btn-xs {{ is_active_sorter('created_at', 'ASC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-up"></i>
            </a>
            <a href="?sortby=count&sortdir=DESC"
               class="btn btn-xs {{ is_active_sorter('created_at', 'DESC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-down"></i>
            </a>
          </div>
        </th>
        <th class="col-xs-3">
          Дата зіпсування
          <div class="pull-right">
            <a href="?sortby=finish_prod&sortdir=ASC"
               class="btn btn-xs {{ is_active_sorter('finish_prod', 'ASC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-up"></i>
            </a>
            <a href="?sortby=finish_prod&sortdir=DESC"
               class="btn btn-xs {{ is_active_sorter('finish_prod', 'DESC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-down"></i>
            </a>
          </div>
        </th>
        <th class="col-xs-2">

        </th>
      </tr>
      </thead>
      <tbody>
      @foreach($products as $prod)
          @if((strtotime($prod->finish_prod) - time()) < 0)
            <tr class="alert-danger">
          @elseif((strtotime($prod->finish_prod) - time()) < 60 * 60 * 24 * 2)
            <tr class="alert-warning">
          @else
            <tr>
          @endif

          <td>{{ $prod->id }}</td>
          <td>
            <a href="/prod/{{ $prod->id }}">{{ $prod->name }}</a>
          </td>
          <td>
              {{ $prod->count }} {{ $prod->count_type }}
          </td>
          <td>
              {{ $prod->finish_prod }}
          </td>
          <td>
            <div class="input-group-btn">
              <a href="{{ route('prod.edit', $prod->id) }}" class="btn btn-primary">Редагувати</a>
              <a href="{{ route('prod.delete', $prod->id) }}" class="btn btn-danger"
                 onclick="return confirm('Впевнені що хочете видалити продукт?');">
                Видалити
              </a>
            </div>
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>
    </div>

    <div class="text-center">
      {{--{!! $pages->appends(request()->except('page'))->links() !!}--}}
    </div>

  </div>
@endsection
