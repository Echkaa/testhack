{!! csrf_field() !!}

<input type="hidden" name="id" id="id" value="{{ old('id', @$prod->id) }}">

<div class="form-group {{ $errors->has('count') ? 'has-error' : '' }}">

  <label for="count" class="control-label">
    Кількість
  </label>

  <div class="row">
    <div class="col-xs-9">
      <input type="number"
           name="count"
             step="0.001"
           id="count"
           value="{{ old('count', @$prod->count) }}"
           placeholder="count"
           class="form-control">
    </div>

    <div class="col-xs-3">
      <select name="count_type" class="form-control" value="{{ old('count_type', @$prod->count_type) }}">
        @foreach($prod->typeCount as $type)
            <option {{ ($prod->count_type == $type) ? 'selected' : '' }} value="{{$type}}">{{$type}}</option>
        @endforeach
      </select>
    </div>
  </div>

  @if ($errors->has('count'))
    <div class="help-block">
      {{ $errors->first('count') }}
    </div>
  @endif
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">

  <label for="name" class="control-label">
    Продукт
  </label>

  <input type="text"
         name="name"
         id="name"
         value="{{ old('name', @$prod->name) }}"
         placeholder="name"
         required
         class="form-control">

  @if ($errors->has('name'))
    <div class="help-block">
      {{ $errors->first('name') }}
    </div>
  @endif
</div>

<div class="form-group {{ $errors->has('created_at') ? 'has-error' : '' }}">

  <label for="created_at" class="control-label">
    Дата додавання
  </label>

  <input type="date"
         name="created_at"
         id="created_at"
         value="{{ old('created_at', @$prod->created_at) }}"
         placeholder="finish_prod"
         disabled="disabled"
         class="form-control">
</div>

<div class="form-group {{ $errors->has('finish_prod') ? 'has-error' : '' }}">

  <label for="finish_prod" class="control-label">
    Дата просрочення
  </label>

  <input type="date"
         name="finish_prod"
         id="finish_prod"
         value="{{ old('finish_prod', @$prod->finish_prod) }}"
         placeholder="finish_prod"
         required
         class="form-control">

  @if ($errors->has('finish_prod'))
    <div class="help-block">
      {{ $errors->first('finish_prod') }}
    </div>
  @endif
</div>

<div style="display: {{ !isset($prod->id) ? 'none' : 'block' }}" class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">

  <label for="content" class="control-label">
    Опис продукту
  </label>

      <textarea
          name="content"
          id="content"
          placeholder="content"
          class="form-control">{{ old('content', @$prod->content) }}</textarea>

  @if ($errors->has('content'))
    <div class="help-block">
      {{ $errors->first('content') }}
    </div>
  @endif
</div>

<div class="form-group">
  <button type="submit" class="btn btn-success">Зберегти</button>
  <a href="/prod" class="btn btn-default">До списку</a>
</div>

