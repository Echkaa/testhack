@extends('layouts.app')

@section('content')
  <div class="container">

    <form method="POST" action="/prod">

      <div class="clearfix">
        <div class="pull-left">
          <div class="lead">
            <strong>Додати новий продукт</strong>
          </div>
        </div>
        <div class="pull-right">
          <button type="submit" class="btn btn-success">Зберегти</button>
          <a href="/prod" class="btn btn-default">До списку</a>
        </div>
      </div>
      <hr>

      @include('prod.form')
    </form>

  </div>
@endsection
