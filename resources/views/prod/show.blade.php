@extends('layouts.app')

@section('content')

  <div class="container">

    <h2>{{ $prod->name }}


      <div class="pull-right">
        <a href="/prod" class="btn btn-default">До списку</a>
      </div></h2>



    <hr>

    <div>
      Кол-во: {{ $prod->count }} {{ $prod->count_type }}</br>
      Дата придбання: {{ $prod->created_at }}</br>
      Срок годности: {{ $prod->finish_prod }}
    </div>

    <hr>

    <div>
      {!! $prod->content !!}
    </div>

  </div>

@endsection
