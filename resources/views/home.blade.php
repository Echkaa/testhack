@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            @if (auth()->check())
              <strong>
                {{ auth()->user()->name }}
              </strong>
            @endif
          </div>

          <div class="panel-body">
            @if (auth()->guest())
              Для доступу до <a href="{{ route('prod.index') }}">сторінки продуктів</a>,
              вам потрібно
              <a href="/register">зареєструвати</a> аккаунт.
              You will automatically logged in after registered.
            @else
              <a href="{{ route('prod.index') }}">Сторінка продуктів</a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
