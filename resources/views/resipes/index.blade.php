@extends('layouts.app')

@section('content')
  <style>
    .alert-danger {
      background-color: #f2dede !important;
    }
    .alert-success {
      background-color: #dff0d8 !important;
    }
  </style>

  <div class="container">

    <div class="clearfix">
      <div class="pull-left">
        <div class="lead">Рецепти</div>
      </div>
      <div class="pull-right">
        <a href="/recipes/add" class="btn btn-success">Додати</a>
      </div>
    </div>

    <hr>

    <div class="col-xs-12" style="overflow-x: auto">
    <table class="table table-bordered table-hover table-striped">
      <thead>
      <tr>
        <th class="col-xs-3">
          Назва рецепту
          <div class="pull-right">
            <a href="?sortby=title&sortdir=ASC"
               class="btn btn-xs {{ is_active_sorter('name', 'ASC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-up"></i>
            </a>
            <a href="?sortby=title&sortdir=DESC"
               class="btn btn-xs {{ is_active_sorter('name', 'DESC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-down"></i>
            </a>
          </div>
        </th>
        <th class="col-xs-3">
          Складність приготування
          <div class="pull-right">
            <a href="?sortby=complexity&sortdir=ASC"
               class="btn btn-xs {{ is_active_sorter('created_at', 'ASC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-up"></i>
            </a>
            <a href="?sortby=complexity&sortdir=DESC"
               class="btn btn-xs {{ is_active_sorter('created_at', 'DESC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-down"></i>
            </a>
          </div>
        </th>
        <th class="col-xs-2">
          Спосіб приготування
          <div class="pull-right">
            <a href="?sortby=cooking_method&sortdir=ASC"
               class="btn btn-xs {{ is_active_sorter('created_at', 'ASC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-up"></i>
            </a>
            <a href="?sortby=cooking_method&sortdir=DESC"
               class="btn btn-xs {{ is_active_sorter('created_at', 'DESC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-down"></i>
            </a>
          </div>
        </th>
        <th class="col-xs-2">
          Можливість приготування
          {{--<div class="pull-right">
            <a href="?sortby=finish_prod&sortdir=ASC"
               class="btn btn-xs {{ is_active_sorter('finish_prod', 'ASC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-up"></i>
            </a>
            <a href="?sortby=finish_prod&sortdir=DESC"
               class="btn btn-xs {{ is_active_sorter('finish_prod', 'DESC') ? 'btn-primary' : 'btn-default' }}">
              <i class="fa fa-arrow-down"></i>
            </a>
          </div>--}}
        </th>
        <th class="col-xs-2">

        </th>
      </tr>
      </thead>
      <tbody>
      @foreach($resipes as $res)
          <?php
          if ($components = $res->components) {
              $can = 'can';
              foreach ($components as $component) {
                  $product = $user->productComponent($component['name']);

                  if (isset($product['name'])) {
                      if ($product['count'] * $coef->typeKoef[$product['count_type']] >= $component['count'] * $coef->typeKoef[$component['count_type']]) {
                          continue;
                      }
                  }

                  if ($component['possibility_of_replacement'] == 'on') {
                      continue;
                  }
                  $can = 'cannot';
              }
          }
          ?>
        <tr class="{{($can == 'cannot') ? 'alert-danger' : 'alert-success'}}">
          <td>
            <a href="/recipes/{{ $res->id }}">{{ $res->title }}</a>
          </td>
          <td>
              {{ $res->complexity }}
          </td>
          <td>
              {{ $res->cooking_method }}
          </td>
          <td>


            {{ ($can == 'cannot') ? 'Недостатньо продуктів' : 'Можливо' }}

          </td>
          <td>
            <div class="input-group-btn">
              <a href="{{ route('recipes.edit', $res->id) }}" class="btn btn-primary">Редагувати</a>
              <a href="{{ route('recipes.delete', $res->id) }}" class="btn btn-danger"
                 onclick="return confirm('Впевнені що хочете видалити рецепт?');">
                Видалити
              </a>
            </div>
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>
    </div>

    <div class="text-center">
      {{--{!! $pages->appends(request()->except('page'))->links() !!}--}}
    </div>

  </div>
@endsection
