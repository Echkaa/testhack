@extends('layouts.app')

@section('content')
  <div class="container">

    <form method="POST" action="/recipes">

      <div class="clearfix">
        <div class="pull-left">
          <div class="lead">
            <strong>Додати новий рецепт</strong>
          </div>
        </div>
        <div class="pull-right">
          <button type="submit" class="btn btn-success">Зберегти</button>
          <a href="/recipes" class="btn btn-default">До списку</a>
        </div>
      </div>
      <hr>

      @include('resipes.form')
    </form>

  </div>
@endsection
