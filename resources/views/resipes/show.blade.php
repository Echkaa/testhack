@extends('layouts.app')

@section('content')
    <style>
        .alert-danger {
            background-color: #f2dede !important;
        }

        .alert-success {
            background-color: #dff0d8 !important;
        }

        .alert-warning {
            background-color: #fcf8e3 !important;
        }
    </style>

    <div class="container">

        <h2>{{ $resipes->title }}


            <div class="pull-right">
                <a href="/recipes" class="btn btn-default">До списку</a>
            </div>
        </h2>

        <hr>

        <table class="table table-bordered table-hover table-striped">
            <tr>
                <th>Інгредієнт</th>
                <th>Кількість</th>
                <th>Можливість змінення</th>
            </tr>
            @if($components = $resipes->components)
                @foreach($components as $component)
                    <?php
                    $can = 'cannot';

                    $product = $user->productComponent($component['name']);

                    if ($component['possibility_of_replacement'] == 'on') {
                        $can = 'maybe';
                    }

                    if (isset($product['name'])) {
                        if ($product['count'] * $coef->typeKoef[$product['count_type']] >= $component['count'] * $coef->typeKoef[$component['count_type']]) {
                            $can = 'can';
                        }
                    }

                    ?>
                    <tr class="{{($can == 'can') ? 'alert-success' : (($can == 'maybe') ? 'alert-warning' : 'alert-danger')}}">
                        <td>{{$component->name}}</td>
                        <td>{{$component->count}} {{$component->count_type}}</td>
                        <td>{{($component->possibility_of_replacement == 'on') ? 'Можливе замінення' : 'Ні'}}</td>
                    </tr>
                @endforeach
            @endif
        </table>

        <hr>

        <div>
            {!! $resipes->recipes !!}
        </div>

    </div>

@endsection
