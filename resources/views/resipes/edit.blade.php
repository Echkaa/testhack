@extends('layouts.app')

@section('content')
  <div class="container">

    <form method="POST" action="/recipes/{{ $resipes->id }}">

      <div class="clearfix">
        <div class="pull-left">
          <div class="lead">
            <strong>Редагування рецепту</strong>
            <small>"{{ $resipes->title }}"</small>
          </div>
        </div>
        <div class="pull-right">
          <button type="submit" class="btn btn-success">Зберегти</button>
          <a href="/recipes" class="btn btn-default">До списку</a>
        </div>
      </div>
      <hr>

      {!! method_field('PUT') !!}
      @include('resipes.form')
    </form>

  </div>
@endsection
