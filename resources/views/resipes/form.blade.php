{!! csrf_field() !!}

<input type="hidden" name="id" id="id" value="{{ old('id', @$resipes->id) }}">

<select style="display: none;" id="count_type">
  @foreach($prod->typeCount as $type)
    <option value="{{$type}}">{{$type}}</option>
  @endforeach
</select>

<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">

  <label for="title" class="control-label">
    Назва рецепту
  </label>

  <input type="text"
         name="title"
         id="title"
         value="{{ old('title', @$resipes->title) }}"
         placeholder="Назва рецепту"
         required
         class="form-control">

  @if ($errors->has('title'))
    <div class="help-block">
      {{ $errors->first('title') }}
    </div>
  @endif
</div>

<div id="components">
  @php
    $i = 1;
  @endphp
  @if($components = $resipes->components)
    @foreach($components as $component)
      <div class="form-group" id="mainBlock{{$i}}">
        <label class="control-label">Інгредієнт {{$i}}</label>
        <input class="form-control" name="components_name[{{$i}}]" placeholder="Назва інгредієнта" style="margin-bottom: 10px;" value="{{$component->name}}">
        <div class="row">
          <div class="col-xs-7">
            <input class="form-control" name="components[{{$i}}]" type="number" placeholder="Кількість інгредієнту" value="{{$component->count}}">
          </div>
          <div class="col-xs-2">
            <select style="display: block;" id="components_type{{$i}}" class="form-control" name="components_type[{{$i}}]">
              @foreach($prod->typeCount as $type)
                <option value="{{$type}}" {{($type == $component->count_type) ? 'selected' : ''}}>{{$type}}</option>
              @endforeach
            </select>
          </div>
          <div class="col-xs-1">
            <input class="form-control" name="components_change[{{$i}}]" type="checkbox" title="Можливість заміни компоненту" id="components_change{{$i}}" {{($component->possibility_of_replacement == 'on') ? 'checked' : ''}}>
          </div>
          <div class="col-xs-2">
              <input style="margin-bottom: 15px" type="button" class="btn btn-danger" value="Видалити" onclick="deleteBlock({{$i}})">
          </div>
        </div>
      </div>
      @php
        $i++;
      @endphp
    @endforeach
  @endif
  <input type="hidden" value="{{$i}}" id="countComponents">
</div>

<input class="btn btn-info" type="button" onclick="add_component();" value="Додати інгредієнт" style="margin-bottom: 20px">
<script>
    var components = document.getElementById("components");
    var length = document.getElementById("countComponents").value;

    function deleteBlock(i) {
        var elem = document.getElementById("mainBlock" + i);
        return elem.parentNode.removeChild(elem);
    }

    function add_component() {
        var new_input = document.createElement("input");
        new_input.classList.add("form-control");
        new_input.name = "components["+length+"]";
        new_input.type = "number";
        new_input.placeholder = "Кількість інгредієнту";

        var new_select = document.getElementById("count_type");
        var new_select_type = new_select.cloneNode(true);
        new_select_type.classList.add("form-control");
        new_select_type.style.display = "block";
        new_select_type.name = "components_type["+length+"]";
        new_select_type.id = "components_type" + length;

        var new_input1 = document.createElement("input");
        new_input1.classList.add("form-control");
        new_input1.name = "components_change["+length+"]";
        new_input1.type = "checkbox";
        new_input1.title = "Можливість заміни компоненту";
        new_input1.id = "components_change" + length;

        var div = document.createElement('div');
        div.classList.add("form-group");
        div.id = "mainBlock" + length;
        div.innerHTML = "<label for='" + new_input.name + "' class='control-label'>Інгредієнт " + length + "</label>";

        var divLittle1 = document.createElement('div');
        divLittle1.classList.add("col-xs-7");
        divLittle1.appendChild(new_input);

        var divLittle2 = document.createElement('div');
        divLittle2.classList.add("col-xs-2");
        divLittle2.appendChild(new_select_type);

        var divLittle3 = document.createElement('div');
        divLittle3.classList.add("col-xs-1");
        divLittle3.appendChild(new_input1);

        var divLittle4 = document.createElement('div');
        divLittle4.classList.add("col-xs-2");
        divLittle4.appendChild(new_input1);
        divLittle4.innerHTML = '<input style="margin-bottom: 15px" type="button" class="btn btn-danger" value="Видалити" onclick="deleteBlock(' + length + ')">';

        var new_input2 = document.createElement("input");
        new_input2.classList.add("form-control");
        new_input2.style.marginBottom = "10px";
        new_input2.name = "components_name["+length+"]";
        new_input2.placeholder = "Назва інгредієнта";

        var divRow = document.createElement('div');
        divRow.classList.add("row");


        divRow.appendChild(divLittle1);
        divRow.appendChild(divLittle2);
        divRow.appendChild(divLittle3);
        divRow.appendChild(divLittle4);

        div.appendChild(new_input2);
        div.appendChild(divRow);

        components.appendChild(div);
        length++;
    } ;
</script>

<div class="form-group {{ $errors->has('complexity') ? 'has-error' : '' }}">

  <label for="complexity" class="control-label">
    Складність рецепту
  </label>

  <select name="complexity" class="form-control" value="{{ old('complexity', @$resipes->complexity) }}">
    @foreach($resipes->complexityTypes as $type)
      <option {{ ($resipes->complexity == $type) ? 'selected' : '' }} value="{{$type}}">{{$type}}</option>
    @endforeach
  </select>
</div>

<div class="form-group {{ $errors->has('cooking_method') ? 'has-error' : '' }}">

  <label for="cooking_method" class="control-label">
    Спосіб приготування
  </label>

  <select name="cooking_method" class="form-control" value="{{ old('cooking_method', @$resipes->cooking_method) }}">
    @foreach($resipes->cookingMethodTypes as $type)
      <option {{ ($resipes->cooking_method == $type) ? 'selected' : '' }} value="{{$type}}">{{$type}}</option>
    @endforeach
  </select>
</div>

<div class="form-group {{ $errors->has('recipes') ? 'has-error' : '' }}">

  <label for="recipes" class="control-label">
    Опис рецепту
  </label>

  <textarea
          name="recipes"
          id="recipes"
          placeholder="recipes"
          class="form-control">{{ old('recipes', @$resipes->recipes) }}</textarea>

  @if ($errors->has('recipes'))
    <div class="help-block">
      {{ $errors->first('recipes') }}
    </div>
  @endif
</div>




<div class="form-group">
  <button type="submit" class="btn btn-success">Зберегти</button>
  <a href="/recipes" class="btn btn-default">До списку</a>
</div>

